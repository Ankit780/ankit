CREATE DATABASE res_production;
USE res_production;
CREATE TABLE user_registration (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
first_name TEXT NOT NULL,
last_name TEXT NOT NULL,
gender TEXT NOT NULL,
date_of_birth INTEGER NOT NULL,
normal_premium_user TEXT NOT NULL,
address TEXT NOT NULL,
phone_no INTEGER NOT NULL);

CREATE TABLE book_details (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT ,
book_title TEXT NOT NULL,
book_author TEXT NOT NULL,
book_publisher TEXT NOT NULL,
book_genre TEXT NOT NULL,
year_of_release INTEGER NOT NULL,
rating INTEGER NOT NULL);

INSERT INTO book_details (book_title,book_author,book_publisher,book_genre,year_of_release,rating)
 VALUES ("Junior Level Books Introduction to Computer","Amit Garg","Reader's Zone"," Computer Science",2011,4.3);
INSERT INTO book_details (book_title,book_author,book_publisher,book_genre,year_of_release,rating)
 VALUES ("Client Server Computing","Sharad Kumar Verma"," Sun India Publications, New Delhi "," Computer Science",2012,4.2);
 INSERT INTO book_details (book_title,book_author,book_publisher,book_genre,year_of_release,rating)
 VALUES ("Harry Potter and the Sorcerer's Stone (Book 1)"," J. K. Rowling"," Sun India Publications, New Delhi "," adventure,magiacal",1998,4.29);
  INSERT INTO book_details (book_title,book_author,book_publisher,book_genre,year_of_release,rating)
 VALUES ("Twilight","Stephenie Meyer"," Little, Brown and Company ","  ‎Romance‎, ‎fantasy‎, ‎young adult fiction",2005,4.3);
  INSERT INTO book_details (book_title,book_author,book_publisher,book_genre,year_of_release,rating)
 VALUES ("The Hobbit","J.R.R. Tolkien"," ‎Peter Jackson‎ ","   ‎High fantasy‎; ‎Juvenile fantasy‎",1997,4);
 
 CREATE TABLE author_details (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 author_name TEXT NOT NULL,
 date_of_birth INTEGER NOT NULL,
 description TEXT NOT NULL);
 
 INSERT INTO author_details (author_name,date_of_birth,description) 
 VALUES ("Amit Garg",1994,"Dr Amit Garg is an Indian mathematician and mental calculator.He currently works as founder and Chief Scientist at ORMAE, a consulting and training firm in Operations Research and Data Science.");
 INSERT INTO author_details (author_name,date_of_birth,description) 
 VALUES ("Sharad Kumar Verma",1995,"Varma was born in 1955 in Satelmond Palace, Poojapura, Thiruvananthapuram. His parents left Kerala and settled down in Madras when he was four.");
 INSERT INTO author_details (author_name,date_of_birth,description) 
 VALUES (" J. K. Rowling",1965 ,"Joanne Rowling,  ( born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, screenwriter, producer, and philanthropist.");
 INSERT INTO author_details (author_name,date_of_birth,description) 
 VALUES ("Stephenie Meyer",1973,"stephenie Meyer_is an American novelist. She is best known for her vampire romance series Twilight, which has sold over 100 million copies, with translations into 37 different languages" );
 INSERT INTO author_details (author_name,date_of_birth,description) 
 VALUES (" ‎Peter Jackson‎ ", 1961,"Sir Peter Robert Jackson (born 31 October 1961) is a New Zealand film director, screenwriter, occasional actor and film producer. He is best known as the director, writer, and producer .");
 
 CREATE TABLE publisher_details (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
 publisher_name TEXT NOT NULL,
 estb_on INTEGER NOT NULL,
 address TEXT NOT NULL,
 description TEXT NOT NULL);
 
  INSERT INTO publisher_details (publisher_name,estb_on,address,description) 
 VALUES("kalyani",1999,"pune","The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals" );
 INSERT INTO publisher_details (publisher_name,estb_on,address,description) 
 VALUES(" Sun India Publications",1998,"newdelhi","The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals" );
 INSERT INTO publisher_details (publisher_name,estb_on,address,description) 
 VALUES(" Sun India Publications",1998,"new delhi","The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals" );
 INSERT INTO publisher_details (publisher_name,estb_on,address,description) 
 VALUES(" Little, Brown and Company",1897,"newyork","Little, Brown and Company is an American publisher founded in 1837 by Charles Coffin Little and his partner, James Brown, and for close to two centuries has published fiction and nonfiction by American authors.");
 INSERT INTO publisher_details (publisher_name,estb_on,address,description) 
 VALUES(" Peter Jackson",1985,"new zealand","Sir Peter Robert Jackson ONZ KNZM (born 31 October 1961) is a New Zealand film director, screenwriter, occasional actor and film producer. ");
 
 CREATE TABLE user_review(
 rented_book TEXT NOT NULL,
liked_book TEXT NOT NULL,
wishlist_added TEXT NOT NULL,
added_friend TEXT NOT NULL);


 
