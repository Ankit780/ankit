-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: res_production
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `res_production`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `res_production` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `res_production`;

--
-- Table structure for table `author_details`
--

DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `author_name` text NOT NULL,
  `date_of_birth` int NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_details`
--

LOCK TABLES `author_details` WRITE;
/*!40000 ALTER TABLE `author_details` DISABLE KEYS */;
INSERT INTO `author_details` VALUES (1,'Amit Garg',1994,'Dr Amit Garg is an Indian mathematician and mental calculator.He currently works as founder and Chief Scientist at ORMAE, a consulting and training firm in Operations Research and Data Science.'),(2,'Sharad Kumar Verma',1995,'Varma was born in 1955 in Satelmond Palace, Poojapura, Thiruvananthapuram. His parents left Kerala and settled down in Madras when he was four.'),(3,' J. K. Rowling',1965,'Joanne Rowling,  ( born 31 July 1965), better known by her pen name J. K. Rowling, is a British author, screenwriter, producer, and philanthropist.'),(4,'Stephenie Meyer',1973,'stephenie Meyer_is an American novelist. She is best known for her vampire romance series Twilight, which has sold over 100 million copies, with translations into 37 different languages'),(5,' ‎Peter Jackson‎ ',1961,'Sir Peter Robert Jackson (born 31 October 1961) is a New Zealand film director, screenwriter, occasional actor and film producer. He is best known as the director, writer, and producer .');
/*!40000 ALTER TABLE `author_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_details`
--

DROP TABLE IF EXISTS `book_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `book_title` text NOT NULL,
  `book_author` text NOT NULL,
  `book_publisher` text NOT NULL,
  `book_genre` text NOT NULL,
  `year_of_release` int NOT NULL,
  `rating` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_details`
--

LOCK TABLES `book_details` WRITE;
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` VALUES (1,'Junior Level Books Introduction to Computer','Amit Garg','Reader\'s Zone','comedy',2011,4),(2,'Client Server Computing','Sharad Kumar Verma',' Sun India Publications, New Delhi ',' Computer Science',2012,4),(3,'Harry Potter and the Sorcerer\'s Stone (Book 1)',' J. K. Rowling',' Sun India Publications, New Delhi ',' adventure,magiacal',1998,4),(4,'Twilight','Stephenie Meyer',' Little, Brown and Company ','  ‎Romance‎, ‎fantasy‎, ‎young adult fiction',2005,4),(5,'The Hobbit','J.R.R. Tolkien',' ‎Peter Jackson‎; ‎Fran Walsh‎ ','   ‎High fantasy‎; ‎Juvenile fantasy‎',1997,4),(6,'Twilight1','Stephenie Meyer1',' Little, Brown and Company 2','  ‎Romance‎, ‎fantasy‎, ‎young adult fiction',2005,4);
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `publisher_name` text NOT NULL,
  `estb_on` int NOT NULL,
  `address` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_details`
--

LOCK TABLES `publisher_details` WRITE;
/*!40000 ALTER TABLE `publisher_details` DISABLE KEYS */;
INSERT INTO `publisher_details` VALUES (1,'kalyani',1999,'pune','The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals'),(2,' Sun India Publications',1998,'newdelhi','The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals'),(3,' Sun India Publications',1998,'new delhi','The Kalyani Group is a privately held industrial group in India. It is focused in four primary sectors, viz. Engineering Steel, Automotive & Non-Automotive Components, Renewable Energy & Infrastructure and Specialty Chemicals'),(4,' Little, Brown and Company',1897,'newyork','Little, Brown and Company is an American publisher founded in 1837 by Charles Coffin Little and his partner, James Brown, and for close to two centuries has published fiction and nonfiction by American authors.'),(5,' Peter Jackson',1985,'new zealand','Sir Peter Robert Jackson ONZ KNZM (born 31 October 1961) is a New Zealand film director, screenwriter, occasional actor and film producer. ');
/*!40000 ALTER TABLE `publisher_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_registration`
--

DROP TABLE IF EXISTS `user_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_registration` (
  `id` int NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `gender` text NOT NULL,
  `date_of_birth` int NOT NULL,
  `normal_premium_user` text NOT NULL,
  `address` text NOT NULL,
  `phone_no` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_registration`
--

LOCK TABLES `user_registration` WRITE;
/*!40000 ALTER TABLE `user_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_review`
--

DROP TABLE IF EXISTS `user_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_review` (
  `rented_book` text NOT NULL,
  `liked_book` text NOT NULL,
  `wishlist_added` text NOT NULL,
  `added_feiend` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_review`
--

LOCK TABLES `user_review` WRITE;
/*!40000 ALTER TABLE `user_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'res_production'
--

--
-- Current Database: `queries`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `queries` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `queries`;

--
-- Table structure for table `user_review`
--

DROP TABLE IF EXISTS `user_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_review` (
  `rented_book` text NOT NULL,
  `liked_book` text NOT NULL,
  `wishlist_added` text NOT NULL,
  `added_friend` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_review`
--

LOCK TABLES `user_review` WRITE;
/*!40000 ALTER TABLE `user_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'queries'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 13:53:43
